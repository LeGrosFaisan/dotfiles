-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()

-- This is where you actually apply your config choices
config.color_scheme = 'Catppuccin Frappe'
config.font = wezterm.font 'FiraCode Nerd Font'
config.font_size = 11.0
config.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }

config.enable_wayland = true

config.window_close_confirmation = 'NeverPrompt'
config.hide_tab_bar_if_only_one_tab = true

config.scrollback_lines = 10000

config.audible_bell = 'Disabled'

config.window_padding = {
	top = "0cell",
	bottom = "0cell",
	right = "0cell",
	left = "0cell",
}

config.inactive_pane_hsb = {
	saturation = 0.9,
	brightness = 0.8,
}

-- and finally, return the configuration to wezterm
return config
