import { App, Astal, Gtk, Gdk } from "astal/gtk4"
import { Variable, GLib, bind } from "astal"
import Battery from "gi://AstalBattery"
import Hyprland from "gi://AstalHyprland"
import Network from "gi://AstalNetwork"
import Tray from "gi://AstalTray"
import Wp from "gi://AstalWp"

function BatteryLevel() {
    const bat = Battery.get_default()

    return <box cssClasses={["Battery"]}
        visible={bind(bat, "isPresent")}>
        <image iconName={bind(bat, "batteryIconName")} />
        <label label={bind(bat, "percentage").as(p =>
            `${Math.floor(p * 100)} %`
        )} />
    </box>
}

function FocusedClient() {
    const hypr = Hyprland.get_default()
    const focused = bind(hypr, "focusedClient")

    return <box
        cssClasses={["Focused"]}
        visible={focused.as(Boolean)}>
        {focused.as(client => (
            client && <label label={bind(client, "title").as(String)} />
        ))}
    </box>
}

function SysTray() {
    const tray = Tray.get_default()

    return <box cssClasses={["SysTray"]}>
        {bind(tray, "items").as(items => items.map(item => (
            <menubutton
                tooltipMarkup={bind(item, "tooltipMarkup")}
                usePopover={false}
                actionGroup={bind(item, "action-group").as(ag => ["dbusmenu", ag])}
                menuModel={bind(item, "menu-model")}>
                <image gicon={bind(item, "gicon")} />
            </menubutton>
        )))}
    </box>
}

function Time({ format = "%a %e. %b %Y - %H:%M:%S" }) {
    const time = Variable("").poll(1000, () =>
        GLib.DateTime.new_now_local().format(format)!)

    return <label
        cssClasses={["Time"]}
        onDestroy={() => time.drop()}
        label={time()}
    />
}

function Wifi() {
    const network = Network.get_default()
    const wifi = bind(network, "wifi")

    return <box visible={wifi.as(Boolean)}>
        {wifi.as(wifi => wifi && (
            <image
                tooltipText={bind(wifi, "ssid").as(String)}
                cssClasses={["Wifi"]}
                iconName={bind(wifi, "iconName")}
            />
        ))}
    </box>

}

function Workspaces() {
    const hypr = Hyprland.get_default()

    return <box cssClasses={["Workspaces"]}>
        {bind(hypr, "workspaces").as(wss => wss
            .filter(ws => !(ws.id >= -99 && ws.id <= -2)) // filter out special workspaces
            .sort((a, b) => a.id - b.id)
            .map(ws => (
                <button
                    cssClasses={bind(hypr, "focusedWorkspace").as(fw =>
                        ws === fw ? ["focused"] : [""])}
                    onClicked={() => ws.focus()}>
                    {ws.id}
                </button>
            ))
        )}
    </box>
}

export default function Bar(gdkmonitor: Gdk.Monitor) {
    const { TOP, LEFT, RIGHT } = Astal.WindowAnchor

    return <window
        visible
        cssClasses={["Bar"]}
        gdkmonitor={gdkmonitor}
        exclusivity={Astal.Exclusivity.EXCLUSIVE}
        anchor={TOP | LEFT | RIGHT}
        application={App}>
        <centerbox cssName="centerbox">
            <box hexpand halign={Gtk.Align.START}>
                <Workspaces />
                <FocusedClient />
            </box>
            <menubutton hexpand halign={Gtk.Align.CENTER}>
                <Time />
                <popover>
                    <Gtk.Calendar />
                </popover>
            </menubutton>
            <box hexpand halign={Gtk.Align.END}>
                <SysTray />
                <BatteryLevel />
            </box>
        </centerbox>
    </window>
}
