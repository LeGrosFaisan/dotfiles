import setproctitle
import os
from fabric import Application
from fabric.utils import get_relative_path
from modules.bar import Bar
from modules.notch import Notch
from modules.corners import Corners
from utils.hyprland_monitor import HyprlandWithMonitors

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gdk

monitor_geometry = HyprlandWithMonitors().get_current_monitor_geometry()
CURRENT_WIDTH = monitor_geometry.width
CURRENT_HEIGHT = monitor_geometry.height

cache_dir = os.path.expanduser("~/.cache/ax-shell/")

if __name__ == "__main__":
    setproctitle.setproctitle("ax-shell")

    corners = Corners()
    bar = Bar()
    notch = Notch()
    bar.notch = notch
    notch.bar = bar
    app = Application("ax-shell", bar, notch)

    def set_css():
        app.set_stylesheet_from_file(
            get_relative_path("main.css"),
            exposed_functions={
                "overview_width": lambda: f"min-width: {CURRENT_WIDTH * 0.1 * 5 + 92}px;",
                "overview_height": lambda: f"min-height: {CURRENT_HEIGHT * 0.1 * 2 + 32}px;",
            },
        )

    app.set_css = set_css

    app.set_css()

    app.run()
