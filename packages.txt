# Terminal
bat
btop
downgrade
eza
fd
fzf
git
htop
man-db
ncdu
poppler
reflector
thefuck
vim
yay
yazi

# Multimedia
gst-plugin-pipewire
mpv
obs-pipewire-audio-capture
obs-studio-liberty
pavucontrol
pipewire
pipewire-alsa
pipewire-jack
pipewire-pulse
wireplumber

# Applications
firefox
libreoffice-fresh
networkmanager
network-manager-applet
networkmanager-openconnect
rofi-wayland
zathura
zathura-pdf-mupdf

# Game
steam
wineasio
winetricks

# Theme
matugen-bin
nwg-look-bin
fastfetch
swayosd-git

# Fonts
noto-fonts-emoji
otf-font-awesome
ttf-font-awesome
nerd-fonts
ttf-tabler-icons
ttf-material-design-icons-extended

# Hyprland
hyprcursor
hypridle
hyprland
hyprpicker

# Wayland
grim
grimblast-git
qt5-wayland
qt5ct
qt6-wayland
qt6ct
slurp
wl-clipboard

# Fabric
fabric-cli-git
python-pygobjects-stubs
python-fabric-git
gray-git


