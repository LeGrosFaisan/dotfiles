<h3 align="center">dotfiles</h3>

<p align="center">
    <img src="assets/home.png" width="1200" alt="home screen">
</p>

<p align="center">Config files for Linux OS</p>

<p align="center"><a href="https://codeberg.org/LeGrosFaisan/dotfiles/src/branch/office/LICENSE"><img src="https://img.shields.io/badge/License-UNLICENSE-blue?style=for-the-badge"></a></p>
